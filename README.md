# csc-syscom-keyring
This is a signed keyring containing the public keys of active syscom members.
This can be used for e.g. verifying their GPG signatures, sending them
encrypted email, etc.

## Importing the keyring
```sh
gpg csc-syscom-keyring.gpg
gpg --import csc-syscom-keyring
```

## Updating the keyring
If you have modified your own key or added someone else's key, make sure
to update the `keys` variable in the csc-syscom-keyring.sh script.

Then run:
```sh
./csc-syscom-keyring.sh
```
