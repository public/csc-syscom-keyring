#!/bin/sh

# Copyright (c) 2021  Amin Bandali <bandali@csclub.uwaterloo.ca>

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.


# GPG key fingerprints of current syscom members
keys="${keys:+${keys}} BE6273738E616D6D1B3A08E8A21A020248816103" # bandali
keys="${keys:+${keys}} 7D0B1775E2626DD899D20BEDDECA0C9D30ED9FE3" # ehashman
keys="${keys:+${keys}} 8E5568ABB0CF96BC367806ED127923BE10DA48DC" # merenber
keys="${keys:+${keys}} E024E07230AFD30042CBE964EF125080E702F60F" # ztseguin
keys="${keys:+${keys}} 64C2FB18E10475229FFF25D1F058DF982DB42F9D" # r389li

printf '\n'

printf '%s' "Backing up the current keyring file..."
mv csc-syscom-keyring.gpg csc-syscom-keyring.gpg.bk
printf '%s\n' " done."

# TODO: implement (optional?) refreshing keys from a keyserver

printf '%s' "Creating new keyring with the given key fingerprints..."
gpg --armor --export $keys > csc-syscom-keyring.gpg
printf '%s\n' " done."

printf '\n'
printf '%s\n' \
       "Before proceeding, please verify (in another terminal) that" \
       "the new refreshed keyring does not contain spam signatures," \
       "by running 'ls -lh csc-syscom-keyring.gpg' and checking that" \
       "the file has a reasonable size (under 1MB)."
printf '\n'
printf '%s' "If the file size looks good, press [Enter] to continue..."
read _
printf '\n'

printf '%s' "Signing the new keyring with your key..."
gpg --armor --sign csc-syscom-keyring.gpg
printf '%s\n' " done."

printf '%s' "Replacing unsigned keyring with signed one..."
mv csc-syscom-keyring.gpg.asc csc-syscom-keyring.gpg
printf '%s\n' " done."

printf '%s' "Removing old backup keyring..."
rm -f csc-syscom-keyring.gpg.bk
printf '%s\n' " done."

printf '\n%s\n' "All done!"
